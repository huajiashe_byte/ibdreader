package mysql_def

/**
*	defalut page size
 */
const UNIV_PAGE_SIZE = 16384 //16k

const SIZE_OF_FIL_HEADER = 38
const SIZE_OF_FIL_TRAILER = 8

/**
*	console command
 */
const SHOW_ALL_PAGES = "show-all-pages"
const SHOW_PAGE = "show-page"
const QUERY_ALL = "query-all"

/**
*	Name string for File Page Types, Reference mysql 8.0 source code description.
*	source code path:
*  	storage/innobase/handler/i_s.cc
*	storage/innobase/include/fil0fil.h
 */

/** File page types (values of FIL_PAGE_TYPE)*/
const (
	/* File page types introduced in MySQL/InnoDB 5.1.7 */
	/** Freshly allocated page */
	FIL_PAGE_TYPE_ALLOCATED = iota

	/** This page type is unused. */
	FIL_PAGE_TYPE_UNUSED

	/** Undo log page */
	FIL_PAGE_UNDO_LOG

	/** Index node */
	FIL_PAGE_INODE

	/** Insert buffer free list */
	FIL_PAGE_IBUF_FREE_LIST

	/** Insert buffer bitmap */
	FIL_PAGE_IBUF_BITMAP

	/** System page */
	FIL_PAGE_TYPE_SYS

	/** Transaction system data */
	FIL_RX_SYS

	/** File space header */
	FIL_PAGE_TYPE_FSP_HDR

	/** Extent descriptor page */
	FIL_PAGE_TYPE_XDES

	/** Uncompressed BLOB page */
	FIL_PAGE_TYPE_BLOB

	/** First compressed BLOB page */
	FIL_PAGE_TYPE_ZBLOB

	/** Subsequent compressed BLOB page */
	FIL_PAGE_TYPE_ZBLOB2

	/** In old tablespaces, garbage in FIL_PAGE_TYPE is replaced with
	  this value when flushing pages. */
	FIL_PAGE_TYPE_UNKNOWN

	/** Compressed page */
	FIL_PAGE_COMPRESSED

	/** Encrypted page */
	FIL_PAGE_ENCRYPTED

	/** Compressed and Encrypted page */
	FIL_PAGE_COMPRESSED_AND_ENCRYPTED

	/** Encrypted R-tree page */
	FIL_PAGE_ENCRYPTED_RTREE

	/** Uncompressed SDI BLOB page */
	FIL_PAGE_SDI_BLOB

	/** Commpressed SDI BLOB page */
	FIL_PAGE_SDI_ZBLOB

	/** Legacy doublewrite buffer page. */
	FIL_PAGE_TYPE_LEGACY_DBLWR

	/** Rollback Segment Array page */
	FIL_PAGE_TYPE_RSEG_ARRAY

	/** Index pages of uncompressed LOB */
	FIL_PAGE_TYPE_LOB_INDEX

	/** Data pages of uncompressed LOB */
	FIL_PAGE_TYPE_LOB_DATA

	/** The first page of an uncompressed LOB */
	FIL_PAGE_TYPE_LOB_FIRST

	/** The first page of a compressed LOB */
	FIL_PAGE_TYPE_ZLOB_FIRST

	/** Data pages of compressed LOB */
	FIL_PAGE_TYPE_ZLOB_DATA

	/** Index pages of compressed LOB. This page contains an array of
	  z_index_entry_t objects.*/
	FIL_PAGE_TYPE_ZLOB_INDEX

	/** Fragment pages of compressed LOB. */
	FIL_PAGE_TYPE_ZLOB_FRAG

	/** Index pages of fragment pages (compressed LOB). */
	FIL_PAGE_TYPE_ZLOB_FRAG_ENTRY
)

const (
	/** Tablespace SDI Index page */
	FIL_PAGE_SDI = iota + 17853

	/** R-tree node */
	RTREE_INDEX //I_S_PAGE_TYPE_RTREE

	/** B-tree node */
	INDEX //FIL_PAGE_INDEX
)

const SIZE_OF_BYTE = 1
const SIZE_OF_SHORT = 2
const SIZE_OF_MEDIUMINT = 3
const SIZE_OF_INT = 4
const SIZE_OF_LONG = 8

var KVS = make(map[int64]string)

func PageType() {
	KVS[FIL_PAGE_TYPE_ALLOCATED] = "FIL_PAGE_TYPE_ALLOCATED"
	KVS[FIL_PAGE_TYPE_UNUSED] = "FIL_PAGE_TYPE_UNUSED"
	KVS[FIL_PAGE_UNDO_LOG] = "FIL_PAGE_UNDO_LOG"
	KVS[FIL_PAGE_INODE] = "FIL_PAGE_INODE"
	KVS[FIL_PAGE_IBUF_FREE_LIST] = "FIL_PAGE_IBUF_FREE_LIST"
	KVS[FIL_PAGE_IBUF_BITMAP] = "FIL_PAGE_IBUF_BITMAP"
	KVS[FIL_PAGE_TYPE_SYS] = "FIL_PAGE_TYPE_SYS"
	KVS[FIL_RX_SYS] = "FIL_RX_SYS"
	KVS[FIL_PAGE_TYPE_FSP_HDR] = "FIL_PAGE_TYPE_FSP_HDR"
	KVS[FIL_PAGE_TYPE_XDES] = "FIL_PAGE_TYPE_XDES"
	KVS[FIL_PAGE_TYPE_BLOB] = "FIL_PAGE_TYPE_BLOB"
	KVS[FIL_PAGE_TYPE_ZBLOB] = "FIL_PAGE_TYPE_ZBLOB"
	KVS[FIL_PAGE_TYPE_ZBLOB2] = "FIL_PAGE_TYPE_ZBLOB2"
	KVS[FIL_PAGE_TYPE_UNKNOWN] = "FIL_PAGE_TYPE_UNKNOWN"
	KVS[FIL_PAGE_COMPRESSED] = "FIL_PAGE_COMPRESSED"
	KVS[FIL_PAGE_ENCRYPTED] = "FIL_PAGE_ENCRYPTED"
	KVS[FIL_PAGE_COMPRESSED_AND_ENCRYPTED] = "FIL_PAGE_COMPRESSED_AND_ENCRYPTED"
	KVS[FIL_PAGE_SDI_BLOB] = "FIL_PAGE_SDI_BLOB"
	KVS[FIL_PAGE_SDI_ZBLOB] = "FIL_PAGE_SDI_ZBLOB"
	KVS[FIL_PAGE_ENCRYPTED_RTREE] = "FIL_PAGE_ENCRYPTED_RTREE"
	KVS[FIL_PAGE_TYPE_LEGACY_DBLWR] = "FIL_PAGE_TYPE_LEGACY_DBLWR"
	KVS[FIL_PAGE_TYPE_RSEG_ARRAY] = "FIL_PAGE_TYPE_RSEG_ARRAY"
	KVS[FIL_PAGE_TYPE_LOB_INDEX] = "FIL_PAGE_TYPE_LOB_INDEX"
	KVS[FIL_PAGE_TYPE_LOB_DATA] = "FIL_PAGE_TYPE_LOB_DATA"
	KVS[FIL_PAGE_TYPE_LOB_FIRST] = "FIL_PAGE_TYPE_LOB_FIRST"
	KVS[FIL_PAGE_TYPE_ZLOB_FIRST] = "FIL_PAGE_TYPE_ZLOB_FIRST"
	KVS[FIL_PAGE_TYPE_ZLOB_DATA] = "FIL_PAGE_TYPE_ZLOB_DATA"
	KVS[FIL_PAGE_TYPE_ZLOB_INDEX] = "FIL_PAGE_TYPE_ZLOB_INDEX"
	KVS[FIL_PAGE_TYPE_ZLOB_FRAG] = "FIL_PAGE_TYPE_ZLOB_FRAG"
	KVS[FIL_PAGE_TYPE_ZLOB_FRAG_ENTRY] = "FIL_PAGE_TYPE_ZLOB_FRAG_ENTRY"
	KVS[FIL_PAGE_SDI] = "FIL_PAGE_SDI"
	KVS[RTREE_INDEX] = "I_S_PAGE_TYPE_RTREE"
	KVS[INDEX] = "FIL_PAGE_INDEX"
}

func ParsePageType(tp int64) string {
	return KVS[tp]
}
