package schema

import (
	"container/list"
	"ibdreader/constants"
)

type TableDef struct {
	FullQualifiedName        string
	Name                     string
	ColumnNames              list.List // List<String>
	ColumnList               list.List // List<Column>
	NameToFieldMap           map[string]Field
	PrimaryKeyMeta           KeyMeta
	SecondaryKeyMetaList     list.List // List<KeyMeta>
	NullableColumnNum        int64
	VariableLengthColumnNum  int64
	NullableColumnList       list.List // List<Column>
	VariableLengthColumnList list.List // List<Column>

	/**
	 * Column ordinal.
	 */
	Ordinal int64

	/**
	 * Default charset for decoding string in Golang. Derived from table DDL default charset
	 * according to {@link CharsetMapping}.
	 */
	DefaultGolangCharset string // = DEFAULT_GOLANG_CHARSET;

	/**
	 * Table DDL default charset, for example can be latin, utf8, utf8mb4.
	 */
	DefaultCharset string // = DEFAULT_MYSQL_CHARSET;

	/**
	 * Table DDL collation, for example can be utf8_general_ci, utf8_bin (case sensitive).
	 */
	Collation string //= DEFAULT_MYSQL_COLLATION;

	/**
	 * Per {@link #collation}, determine if string type columns are case sensitive or not.
	 */
	CollationCaseSensitive bool // = false

	/**
	 * //TODO make sure this is the right way to implement
	 * For example, if table charset set to utf8, then it will consume up to 3 bytes for one character.
	 * if it is utf8mb4, then it must be set to 4.
	 */
	MaxBytesPerChar int64 // = 1

	/**
	 * When build new TableDef for secondary key, this indicates that the table
	 * is derived from sk, and will only by used internally.
	 */
	DerivedFromSk bool
}

func NewTableDef() TableDef {
	td := TableDef{
		FullQualifiedName:        "",
		Name:                     "",
		ColumnNames:              *list.New(),
		ColumnList:               *list.New(),
		NameToFieldMap:           make(map[string]Field),
		PrimaryKeyMeta:           KeyMeta{},
		SecondaryKeyMetaList:     *list.New(),
		NullableColumnNum:        0,
		VariableLengthColumnNum:  0,
		NullableColumnList:       *list.New(),
		VariableLengthColumnList: *list.New(),
		Ordinal:                  0,
		DefaultGolangCharset:     constants.DEFAULT_GOLANG_CHARSET,
		DefaultCharset:           constants.DEFAULT_MYSQL_CHARSET,
		Collation:                constants.DEFAULT_MYSQL_COLLATION,
		CollationCaseSensitive:   false,
		MaxBytesPerChar:          1,
		DerivedFromSk:            false, //bool
	}
	return td
}

type Field struct {
	ordinal int64
	name    string
	column  Column
}
