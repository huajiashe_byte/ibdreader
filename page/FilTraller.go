package page

import (
	"ibdreader/slice"
)

/*
	https://dev.mysql.com/doc/internals/en/innodb-fil-trailer.html
	The Fil Trailer has one part, as follows: (8 byte):
	+-----------------------+------+----------------------------------------------------------------------+
	+	Name 				+ Size + Remarks														  	  + 										   +
	+-----------------------+------+----------------------------------------------------------------------+
	+	FIL_PAGE_END_LSN 	+  8   + low 4 bytes = checksum of page, last 4 bytes = same as FIL_PAGE_LSN  +
	+-----------------------+------+----------------------------------------------------------------------+
*/

type FilTrailer struct {
	Checksum int64
	Low32lsn int64
}

func FilTrailerReadSlice(pos int64) FilTrailer {
	var ft FilTrailer
	slice.SetPosition(pos) // last 8 byte
	ft.Checksum = slice.ReadUnsignedInt()
	ft.Low32lsn = slice.ReadUnsignedInt()
	return ft
}
