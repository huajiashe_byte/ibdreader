package page

import "ibdreader/slice"

type ListBaseNode struct {
	/**
	 * 存储链表的长度
	 */
	Length int64

	/**
	 * 指向链表的第一个节点
	 */
	FirstPageNumber int64
	FirstOffset     int64

	/**
	 * 指向链表的最后一个节点
	 */
	LastPageNumber int64
	LastOffset     int64
}

func ListBaseNodefromSlice() ListBaseNode {
	var lbn ListBaseNode
	lbn.Length = slice.ReadUnsignedInt()
	lbn.FirstPageNumber = slice.IfUndefined(slice.ReadUnsignedInt())
	lbn.FirstOffset = slice.ReadUnsignedShort()
	lbn.LastPageNumber = slice.IfUndefined(slice.ReadUnsignedInt())
	lbn.LastOffset = slice.ReadUnsignedShort()
	return lbn
}
