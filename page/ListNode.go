package page

import "ibdreader/slice"

type ListNode struct {
	/**
	 * 指向当前节点的前一个节点
	 */
	PrevPageNumber int64
	PrevOffset     int64

	/**
	 * 指向当前节点的下一个节点
	 */
	NextPageNumber int64
	NextOffset     int64
}

func ListNodeReadSlice() ListNode {
	var ln ListNode
	ln.PrevPageNumber = slice.ReadUnsignedInt()
	ln.PrevOffset = slice.ReadShortInt16()
	ln.NextPageNumber = slice.ReadUnsignedInt()
	ln.NextOffset = slice.ReadShortInt16()
	return ln
}
