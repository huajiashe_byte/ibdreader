package page

type PageDirection string

const (
	LEFT         = 1 // Inserts have been in descending order
	RIGHT        = 2 // Inserts have been in ascending order
	SAME_REC     = 3 // Unused by InnoDB
	SAME_PAGE    = 4 // Unused by InnoDB
	NO_DIRECTION = 5 // Inserts have been in random order
)
