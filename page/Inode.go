package page

import (
	"container/list"
	"ibdreader/mysql_def"
	"ibdreader/slice"
)

const MAX_INODE_ENTRY_SIZE = 85

type Inode struct {
	InodeEntryList list.List
}

func InodeReadSlice() Inode {
	var ino Inode
	slice.SetPosition(mysql_def.SIZE_OF_FIL_HEADER + 12)
	for i := 0; i < MAX_INODE_ENTRY_SIZE; i++ {
		ie := InodeEntryReadSlice()
		if ie.MagicNumber == 0 || ie.SegmentId == 0 {
			break
		}
		ino.InodeEntryList.PushBack(ie)
	}
	return ino
}
