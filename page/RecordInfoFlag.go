package page

type RecordInfoFlag string

const MIN_REC = 1       // this record is the minimum record in a non-leaf level of the B+Tree
const DELETE_MARKED = 2 // this record will be actually deleted by a purge operation in the future
