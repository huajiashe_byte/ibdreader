package page

import (
	"ibdreader/mysql_def"
	"ibdreader/slice"
)

/**
 * page = FIL HEADER (38) + body + FIL TRAILER(8).
 *
 */
type InnerPage struct {
	FilHeader  FilHeader
	FilTrailer FilTrailer

	/**
	 * page 0 is located at file offset 0, page 1 at file offset 16384.
	 */
	PageNumber int64
}

/**
 * data is 16k page byte buffer with fil header and fil trailer.
 */
func InnerPageReadSlice(pageNum int64) InnerPage {
	var ip InnerPage
	ip.PageNumber = pageNum
	ip.FilHeader = FileHeaderReadSlice(0)
	ip.FilTrailer = FilTrailerReadSlice(16376)
	slice.SetPosition(mysql_def.SIZE_OF_FIL_HEADER)
	return ip
}
